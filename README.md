The purpose of this board is to propose an innovative and low cost power topology using
two sensitive SCRs and a single IGBT to dim all kind of lamps: 100 - 240 V halogen lamps,
SELV halogen lamps through magnetic or electronic transformers, and the new CFL and
LED dimmable lamps.

The main features of this dimmer are:

• Operation for 2-wire wall dimmer
• Leading-edge control only (compatible with all lamps commonly found on the shelves)
• Operation on 110 V or 230 V line rms voltage and 50 Hz or 60 Hz line frequency
• Dimmable power range (note: higher power is possible with larger heatsink):

– 3 to 600 W for 230 V rms line
– 3 to 300 W for 110 V rms line
– power efficiency @ 230 V > 99%
– standby losses @ 230 V < 0.3 W

• Short-circuit protection at startup
• Enhanced interface with pushbuttons; soft-start and soft-stop; memory of last setting
• Compliance with EMC standards:

– Compliant with EN55015 (for European market)
– Criteria A for 2 kV IEC 61000-4-5 surge for fast transients above 2.5 kV according to
IEC 61000-4-4)